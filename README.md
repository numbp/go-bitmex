# get websocket for bitmex

- golang
- websocket

## Usage

```
import "github.com/zmxv/bitmexgo"


func main() {
    c := bitmex.New(key, SecretKey)
	_, _, err := c.C.StatsApi.StatsGet(c.Auth)
	if err != nil {
		t.Error(err)
	}
	// fmt.Printf("%+v\n", res)
	// fmt.Printf("%+v\n", status)

	ch := make(chan Response)
	subscrives := []string{`"funding:XBT"`, `"liquidation:XBT"`, `"liquidation:ETH"`, `"orderBookL2_25:XBTUSD"`, `"trade:XBTUSD"`, `"tradeBin1m:XBTUSD"`} //, `"execution"`, `"order"`, `"margin"`, `"position"`, `"wallet"`}

	go Get(subscrives, ch)

	for {
		select {
		case v := <-ch:
			switch v.Type {
			case ws.TypeOrderbook:
				fmt.Printf("キタコレ%+v\n", v.Orderbook)
			}
		}
	}
}
```