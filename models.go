package bitmex

import (
	"context"

	"github.com/zmxv/bitmexgo"
)

type Bitmex struct {
	Auth context.Context
	C    *bitmexgo.APIClient
}

func New(key, secret string) *Bitmex {
	return &Bitmex{
		Auth: bitmexgo.NewAPIKeyContext(key, secret),
		C:    bitmexgo.NewAPIClient(bitmexgo.NewConfiguration()),
	}
}
