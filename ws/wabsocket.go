package ws

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/labstack/gommon/log"

	"github.com/zmxv/bitmexgo"
	"golang.org/x/net/websocket"
)

const (
	Error GetType = iota
	TypeFunding
	TypeLiquidation
	TypeOrderbook
	TypeQuote
	TypeTrade
	TypeTradeBin
	TypeSettlement
	TypeExecution
	TypeOrder
	TypeMargin
	TypePosition
	TypeWallet
)

type GetType int

type Response struct {
	sync.Mutex

	Type  GetType
	Error error

	E Evaluation

	Funding     []bitmexgo.Funding
	Liquidation struct {
		Rekts     []bitmexgo.Liquidation
		CreatedAt time.Time
	}
	Orderbook  []bitmexgo.OrderBookL2
	Quote      []bitmexgo.Quote
	Trade      []bitmexgo.Trade
	TradeBin   []bitmexgo.TradeBin
	Settlement []bitmexgo.Settlement

	// Private
	Execution []bitmexgo.Execution
	Order     []bitmexgo.Order
	Margin    []bitmexgo.Margin
	Position  []bitmexgo.Position
	Wallet    bitmexgo.Wallet
}

type Success struct {
	Success   bool   `json:"success"`
	Subscribe string `json:"subscribe"`
	Request   struct {
		Op   string   `json:"op"`
		Args []string `json:"args"`
	} `json:"request"`
}

type R struct {
	Table string   `json:"table"`
	Keys  []string `json:"keys"`
	Types struct {
		ID     string `json:"id"`
		Price  string `json:"price"`
		Side   string `json:"side"`
		Size   string `json:"size"`
		Symbol string `json:"symbol"`
	} `json:"Types"`
	ForeignKeys struct {
		Side   string `json:"side"`
		Symbol string `json:"symbol"`
	} `json:"foreignKeys"`
	Attributes struct {
		ID     string `json:"id"`
		Symbol string `json:"symbol"`
	} `json:"attributes"`
	Action string `json:"action"`
	Data   []map[string]interface {
	} `json:"data"`
}

func Get(subscripts []string, ch chan Response) {
	var (
		res Response

		origin = "https://www.bitmex.com"
		url    = "wss://www.bitmex.com/realtime"
	)
	ws, err := websocket.Dial(url, "", origin)
	if err != nil {
		// s := &Response{
		// 	Error: errors.Wrap(err, "websocket connecting error: "),
		// }
		// ch <- *s
	}
	defer ws.Close()

	set := fmt.Sprintf(`{"op": "subscribe", "args": [%s]}`, strings.Join(subscripts, ","))

	if _, err := ws.Write([]byte(set)); err != nil {
		log.Error(err)
		return
	}

	var msg = make([]byte, 512)
	for {
		if err = websocket.Message.Receive(ws, &msg); err != nil {
			continue
		}

		switch {
		case bytes.HasPrefix(msg, []byte(`{"success":`)):
			var s Success
			if err := json.Unmarshal(msg, &s); err != nil {
				log.Debug(err)
				return
			}

			fmt.Printf("ws connected success: %v, subscribe [%s]\n", s.Success, s.Subscribe)

		case bytes.HasPrefix(msg, []byte(`{"table":`)):
			var r R
			if err := json.Unmarshal(msg, &r); err != nil {
				log.Debug(err)
				return
			}
			switch {
			case strings.HasPrefix(r.Table, "announcement"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "chat"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "connected"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "funding"):
				res.Unmarshal(TypeFunding, r)
				ch <- res

			case strings.HasPrefix(r.Table, "instrument"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "insurance"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "liquidation"):
				res.Unmarshal(TypeLiquidation, r)
				ch <- res
			case strings.HasPrefix(r.Table, "orderBookL2_25"):
				res.Unmarshal(TypeOrderbook, r)
				ch <- res
			case strings.HasPrefix(r.Table, "orderBookL2"):
				res.Unmarshal(TypeOrderbook, r)
				ch <- res
			case strings.HasPrefix(r.Table, "orderBook10"):
				res.Unmarshal(TypeOrderbook, r)
				ch <- res
			case strings.HasPrefix(r.Table, "publicNotifications"):
				fmt.Printf("has table: %+v\n", r.Table)
				ch <- res
			case strings.HasPrefix(r.Table, "quoteBin"):
				res.Unmarshal(TypeQuote, r)
				ch <- res
			case strings.HasPrefix(r.Table, "quote"):
				res.Unmarshal(TypeQuote, r)
				ch <- res

			case strings.HasPrefix(r.Table, "settlement"):
				res.Unmarshal(TypeSettlement, r)
				ch <- res

			case strings.HasPrefix(r.Table, "tradeBin"):
				res.Unmarshal(TypeTradeBin, r)
				ch <- res
			case strings.HasPrefix(r.Table, "trade"):
				res.Unmarshal(TypeTrade, r)
				ch <- res

			// Private
			case strings.HasPrefix(r.Table, "affiliate"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "execution"):
				res.Unmarshal(TypeExecution, r)
				ch <- res
			case strings.HasPrefix(r.Table, "order"):
				res.Unmarshal(TypeOrder, r)
				ch <- res
			case strings.HasPrefix(r.Table, "margin"):
				res.Unmarshal(TypeMargin, r)
				ch <- res
			case strings.HasPrefix(r.Table, "position"):
				res.Unmarshal(TypePosition, r)
				ch <- res
			case strings.HasPrefix(r.Table, "privateNotifications"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "transact"):
				fmt.Printf("has table: %+v\n", r.Table)
			case strings.HasPrefix(r.Table, "wallet"):
				res.Unmarshal(TypeWallet, r)
				ch <- res

			default:
				fmt.Println("ws table Type is not supposed")
			}

		default:
			fmt.Println("ws response is not supposed")
		}
	}
}

func (p *Response) Unmarshal(t GetType, r R) error {
	p.Lock()
	defer p.Unlock()

	b, err := json.Marshal(r.Data)
	if err != nil {
		return err
	}

	switch t {
	case Error:
	case TypeFunding:
		json.Unmarshal(b, &p.Funding)
		p.Type = TypeFunding

	case TypeLiquidation:
		json.Unmarshal(b, &p.Liquidation.Rekts)
		// 配信発動がわからないためタイムスタンプを自作
		p.Liquidation.CreatedAt = time.Now()
		p.Type = TypeLiquidation

	case TypeOrderbook:
		json.Unmarshal(b, &p.Orderbook)
		p.Type = TypeOrderbook

	case TypeQuote:
		json.Unmarshal(b, &p.Quote)
		p.Type = TypeQuote

	case TypeSettlement:
		json.Unmarshal(b, &p.Settlement)
		p.Type = TypeSettlement

	case TypeTrade:
		json.Unmarshal(b, &p.Trade)
		p.Type = TypeTrade
	case TypeTradeBin:
		json.Unmarshal(b, &p.TradeBin)
		p.Type = TypeTradeBin

	case TypeExecution:
		json.Unmarshal(b, &p.Execution)
		p.Type = TypeExecution
	case TypeOrder:
		json.Unmarshal(b, &p.Order)
		p.Type = TypeOrder
	case TypeMargin:
		json.Unmarshal(b, &p.Margin)
		p.Type = TypeMargin
	case TypePosition:
		json.Unmarshal(b, &p.Position)
		p.Type = TypePosition
	case TypeWallet:
		json.Unmarshal(b, &p.Wallet)
		p.Type = TypeWallet

	default:
		return errors.New("has Type not suported")
	}

	// p.Culc()

	return nil
}

type Evaluation struct {
	// TradeExec
	ExecLastSize int
}

func (p *Response) Culc() {
	switch p.Type {
	case TypeTrade:
		// 市場直近の執行取引
		var sum int
		for _, v := range p.Trade {
			if "BUY" == strings.ToUpper(v.Side) {
				sum += v.Size
			} else if "SELL" == strings.ToUpper(v.Side) {
				sum -= v.Size
			}
		}
		p.E.ExecLastSize = sum

	case TypeOrderbook:
		for _, v := range p.Orderbook {
			toPrice := true
			_ = IDcomegoPrice(toPrice, float64(v.Id))
			_ = IDcomegoPrice(false, v.Price)
			// fmt.Printf("culc id: %.1f, id: %d, price: %.1f, size: %d\n", id, v.Id, v.Price, v.Size)
			// fmt.Printf("culc price: %.1f, id: %d, price: %.1f, size: %d\n", price, v.Id, v.Price, v.Size)
		}
	}
}

// TODO:
func IDcomegoPrice(toPrice bool, in float64) float64 {
	xbtID := 88.0 // 2019/02/16
	e8 := 100000000.0
	if toPrice {
		// id to price
		toXBT := 0.01
		return ((e8 * xbtID) - in) * toXBT
	}

	// price to id
	toXBT := 0.05
	return 100000000*xbtID - (in / toXBT)
}
