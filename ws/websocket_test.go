package ws

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/k-terashima/go-bitmex"

	"github.com/BurntSushi/toml"
)

type Config struct {
	Key        string
	SecretKey  string
	KeyM       string
	SecretKeyM string

	Size float64
}

func load() (Config, error) {
	var c Config
	if _, err := toml.DecodeFile("", &c); err != nil {
		return c, errors.New("config file not finds")
	}
	fmt.Printf("%+v\n", c)
	return c, nil
}

func TestGetConnect(t *testing.T) {
	a, _ := load()
	c := bitmex.New(a.KeyM, a.SecretKeyM)
	_, _, err := c.C.StatsApi.StatsGet(c.Auth)
	if err != nil {
		t.Error(err)
	}
	// fmt.Printf("%+v\n", res)
	// fmt.Printf("%+v\n", status)

	ch := make(chan Response)
	subscrives := []string{`"funding:XBT"`, `"orderBookL2_25:XBTUSD"`, `"trade:XBTUSD"`, `"tradeBin1m:XBTUSD"`} //, `"execution"`, `"order"`, `"margin"`, `"position"`, `"wallet"`}

	go Get(subscrives, ch)

	for {
		select {
		case v := <-ch:
			switch v.Type {
			case TypeOrderbook:
				fmt.Printf("キタコレ%+v\n", v.Orderbook)
			}
		}
	}
}
